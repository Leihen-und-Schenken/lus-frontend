import { createApp } from 'vue'
import { createApolloProvider } from '@vue/apollo-option'
import { createPinia } from 'pinia'
import { defineStore } from 'pinia'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import { ApolloClient, InMemoryCache } from '@apollo/client/core'
import VueApolloComponents from '@vue/apollo-components'
import { HttpLink } from "apollo-link-http";
import { ApolloLink, concat } from "apollo-link";


const pinia = createPinia()

export const useUserStore = defineStore('main', {
  state: () => ({
    username: null,
    email: null,
    accessToken: null,
    loggedIn: false,
    nav_items: [
      "home",
      "login",
    ],
  }),
  getters: {
  },
  actions: {
    login(mutation) {
      let result = mutation.data.userPasswordAuthenticationMutation
      if (result.success) {
        this.accessToken = result.accessToken
        this.loggedIn = true
        this.nav_items = [
          "home",
          "myItems",
          "items",
          "createItem",
        ]
      }
      return result.success
    },
    logout() {
      this.username = false
      this.email = false
      this.accessToken = false
      this.loggedIn = false
      this.nav_items = [
        "home",
        "login",
      ]
    }
  },
})

const httpLink = new HttpLink({ uri: process.env.VUE_APP_LUS_GRAPHQL_ENDPOINT });
const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      authorization: useUserStore().accessToken ? `Bearer ${useUserStore().accessToken}` : "",
    },
  });
  return forward(operation);
});

export const apolloClient = new ApolloClient({
  link: concat(authMiddleware, httpLink),
  cache: new InMemoryCache(),
});

const apolloProvider = createApolloProvider({
  defaultClient: apolloClient,
})

createApp(App)
  .use(apolloProvider)
  .use(pinia)
  .use(router)
  .use(VueApolloComponents)
  .use(vuetify)
  .mount('#app')
