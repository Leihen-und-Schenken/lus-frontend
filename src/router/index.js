import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ItemCreateView from '../views/ItemCreateView.vue'
import ItemDetailView from '../views/ItemDetailView.vue'
import ItemsListView from '../views/ItemsListView.vue'
import MyItemsListView from '../views/MyItemsListView.vue'
import LoginView from '../views/LoginView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login/',
    name: 'login',
    component: LoginView
  },
  {
    path: '/items/my',
    name: 'myItems',
    component: MyItemsListView
  },
  {
    path: '/items/',
    name: 'items',
    component: ItemsListView
  },
  {
    path: '/items/:uuid/',
    name: 'item',
    component: ItemDetailView
  },
  {
    path: '/items/create/',
    name: 'createItem',
    component: ItemCreateView
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
